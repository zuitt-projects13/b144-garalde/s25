db.fruits.aggregate([
    { $unwind : "$origin" },
    { $group : { _id: "$origin", numOfFruits: { $sum: "$stock" }}}
])